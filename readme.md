# Java toolchain

Simple scripts for building, testing and auditing Java projects
on the console without the need of a full-fledged IDE

## Install

Copy the scripts to a location in your path (preferaly ~/bin/ or something)
and make them executable

## Prerequisites

You will need the bourne again shell (or some compliant shell), sed, grep, diff
and of course Java.
Auditing needs Checkstyle, PMD and CPD. You can change their locations in javatc-audit, but the default locations are:

* Checkstyle: ~/bin/checkstyle.jar
* PMD/CPD: ~/bin/pmd/

## Usage

Init a new project by navigating to the new dir and executing
`javatc-init <package>`.
This will set up the ./src, ./test and ./out dir, as well as a makefile and will init an empty git repo. 

### Build

Run `make build` or `javatc-build <package>`. This will build the project and run tests. Tests are run against the specified package and will be feeded to a Main and Test class.

### Audit

Run `make audit` or `javatc-audit` to audit the project. Checkstle will use a file named checkstyle.xml in the project dir. All results are sent to the ./out dir.

### Test

Every test consists of a <Name>.test and a <Name>.exp file in the ./test dir. Every line from <Name>.test is feeded via stdin to <package>.Main and stdout is sent to ./out/test/<Name>.out . ./out/test.diff will contain a diff of the expected output in ./test/<Name>.exp and the ./out/test/<Name>.out file.

Additionally, <package>.Test will be invoked and its output is went to ./out/test.out

